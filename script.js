

function schimbaPagina(paginaNoua){
    $(".pagina").hide();
    $(`#${paginaNoua}`).show();
}





$(document).ready(function(){
    checkCookie();
    var c = document.getElementById("semnInterzis");
    var ctx = c.getContext("2d");
    ctx.font = "30px Arial";
    ctx.strokeText("Acces interzis!", 10, 50);
})





function scrieInLocalStorage(){
    tehnologii = [
        "Faza lunga adaptiva",
        "Cornering",
        "Stergatoare automate",
        "Faruri laser",
        "Cutie de viteza hidramata",
        "Stopuri led",
        "Motor Mild Hybrid"
    ]
    localStorage.setItem('tehnologii', tehnologii)
}


function citesteDinLocalStorage(){
    tehnologii = localStorage.getItem('tehnologii')
    return tehnologii
}

function populeazaTabelTehnologii(){
    scrieInLocalStorage()
    html = ""
    tehnologii = citesteDinLocalStorage()
    tehnologii = tehnologii.split(',')
    for(i=0; i < tehnologii.length; i++ ){
        html += `
            <tr>
                <th scope="row">${i+1}</th>
                <td>${tehnologii[i]}</td>
            </tr>
             `
    }
    $("#tabel-tehnologii").html(html)

}





function citesteNumeleVizitatorului(){
    Swal.fire({
        title: 'Introduceti numele dumneavoastra',
        input: 'text',
        // showCancelButton: true,
        confirmButtonText: 'Confirma',
        showLoaderOnConfirm: true,
        preConfirm: (nume) => {
          sessionStorage.setItem("nume", nume)
          var d = new Date();
          d.setTime(d.getTime() + (1*24*60*60*1000));
          var expires = "expires="+ d.toUTCString();
          document.cookie = `nume=${nume};${expires};path=/`;
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        if (result.isConfirmed) {
            nume = sessionStorage.getItem("nume")
          Swal.fire({
            title: `Bine ai venit ${nume}`,
            imageUrl: '/carguys.png'
          })
        }
      })
}



function checkCookie() {
    var nume = getCookie("nume");
    if (nume != "") {
        Swal.fire({
            title: `Bine ai revenit ${nume}`,
            imageUrl: '/carguys.png'
          })
    } else {
      citesteNumeleVizitatorului()
    }
  }

  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }